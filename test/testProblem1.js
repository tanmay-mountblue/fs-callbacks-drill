const path = require("path");
const problem1 = require("../problem1");
const callback = (error, success) => {
  if (error.length || error) {
    console.error("Folowing Failures Occured");
    console.error(error.message || error);
  }
  if (success && success.length) {
    console.log("Following Success Executions");
    console.log(success);
  }
};

/***
 * Test Case 1
 */
let err = problem1(path.join(__dirname, "../testDir"), 5, callback);
if (err) {
  console.error("Failure");
  console.error(err.message || err);
}

/***
 * Test Case 2 - Inavlid Callback
 */
let err2 = problem1(path.join(__dirname, "../testDir"), 5, []);
if (err2) {
  console.error("Failure");
  console.error(err2.message || err2);
}

/***
 * Test Case 3 - Inavlid Input
 */
let err3 = problem1(path.join(__dirname, "../testDir"), [], callback);
if (err3) {
  console.error("Failure");
  console.error(err3.message || err3);
}
