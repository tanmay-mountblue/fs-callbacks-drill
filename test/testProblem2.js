const path = require("path");
const problem2 = require("../problem2");

/***
 * Test Case 1
 */
let err = problem2((err, data) => {
  if (err || err.length > 0) {
    console.error("Failure");
    console.error(err.message || err);
  }
  if (data.length > 0) {
    console.log("Success");
    console.log(data);
  }
});
if (err) {
  console.error("Failure");
  console.error(err);
}

/***
 * Test Case 2
 */
let err1 = problem2([]);
if (err1) {
  console.error(err1.message);
}
