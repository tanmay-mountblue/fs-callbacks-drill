const fs = require("fs");

module.exports = (dirPath, numOfFiles, callback) => {
  if (!callback || typeof callback !== "function") {
    return new Error("Callback is not a function!!!");
  } else if (
    !dirPath ||
    typeof dirPath !== "string" ||
    !numOfFiles ||
    typeof numOfFiles !== "number"
  ) {
    callback(new Error("Invalid Input!!!"));
  } else {
    fs.mkdir(dirPath, { recursive: true }, (err) => {
      if (err) {
        callback(err.message);
      } else {
        let addErrorLogs = [],
          addSuccessLogs = [],
          deleteSuccessLogs = [],
          deleteErrorLogs = [],
          callbackExecCount = 0;

        for (let fileNumber = 0; fileNumber <= numOfFiles - 1; fileNumber++) {
          fs.writeFile(`${dirPath}/file${fileNumber}.json`, "Hello", (err) => {
            if (err) {
              addErrorLogs.push(err.message);
            } else {
              addSuccessLogs.push("Added File" + fileNumber +" Successfully");
              setTimeout(() => {
                fs.unlink(`${dirPath}/file${fileNumber}.json`, (err) => {
                  callbackExecCount++;

                  if (err) {
                    deleteErrorLogs.push(err.message);
                  } else {
                    deleteSuccessLogs.push("Deleted File" + fileNumber);
                  }

                  if (addSuccessLogs.length === callbackExecCount) {
                    callback(
                      addErrorLogs.concat(deleteErrorLogs),
                      addSuccessLogs.concat(deleteSuccessLogs)
                    );
                  }
                });
              }, 2 * 1000);
            }

            if (
              fileNumber === numOfFiles - 1 &&
              addErrorLogs.length === numOfFiles &&
              addSuccessLogs.length === 0
            ) {
              callback(addErrorLogs, addSuccessLogs);
            }
          });
        }
      }
    });
  }
};
