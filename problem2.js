const fs = require("fs");
const path = require("path");
const folderPath = path.join(__dirname, "./problem2Data");
const lipsumFilePath = path.join(__dirname, "./data/lipsum.txt");

module.exports = (cb) => {
  if (typeof cb !== "function") {
    return new Error("Invalid Input!!!");
  } else {
    let fileCount = 1,
      successLogs = [],
      failureLogs = [];
    fs.readFile(lipsumFilePath, "utf8", (err, data) => {
      if (err) {
        cb(err);
      } else {
        successLogs.push("Read file lipsum.txt Successfull");
        fs.writeFile(
          `${folderPath}/${fileCount}.txt`,
          data.toUpperCase(),
          (err) => {
            if (err) {
              cb(err, successLogs);
            } else {
              successLogs.push(`Write file ${fileCount} Successfull`);

              fs.writeFile(
                `${folderPath}/filenames.txt`,
                fileCount + ".txt",
                (err) => {
                  if (err) {
                    cb(err, successLogs);
                  } else {
                    successLogs.push("Write file filenames.txt Successfull");

                    fs.readFile(
                      `${folderPath}/${fileCount}.txt`,
                      "utf8",
                      (err, data) => {
                        if (err) {
                          cb(err, successLogs);
                        } else {
                          successLogs.push(
                            `Read file ${fileCount} Successfull`
                          );

                          let sentences = data.toLowerCase().split(". ");
                          fileCount++;
                          fs.writeFile(
                            `${folderPath}/${fileCount}.txt`,
                            JSON.stringify(sentences),
                            (err) => {
                              if (err) {
                                cb(err, successLogs);
                              } else {
                                successLogs.push(
                                  `Write file ${fileCount} Successfull`
                                );

                                fs.appendFile(
                                  `${folderPath}/filenames.txt`,
                                  `\n${fileCount}.txt`,
                                  (err) => {
                                    if (err) {
                                      cb(err, successLogs);
                                    } else {
                                      successLogs.push(
                                        `Append file filenames.txt Successfull`
                                      );

                                      fs.readFile(
                                        `${folderPath}/${fileCount}.txt`,
                                        "utf8",
                                        (err, data) => {
                                          if (err) {
                                            cb(err, successLogs);
                                          } else {
                                            successLogs.push(
                                              `Read file ${fileCount} Successfull`
                                            );

                                            dataArray = JSON.parse(data);
                                            dataArray.sort();
                                            fileCount++;
                                            fs.writeFile(
                                              `${folderPath}/${fileCount}.txt`,
                                              JSON.stringify(dataArray),
                                              (err) => {
                                                if (err) {
                                                  cb(err, successLogs);
                                                } else {
                                                  fs.appendFile(
                                                    `${folderPath}/filenames.txt`,
                                                    `\n${fileCount}.txt`,
                                                    (err) => {
                                                      if (err) {
                                                        cb(err, successLogs);
                                                      } else {
                                                        successLogs.push(
                                                          `Append file filenames.txt Successfull`
                                                        );

                                                        fs.readFile(
                                                          `${folderPath}/filenames.txt`,
                                                          "utf-8",
                                                          (err, data) => {
                                                            if (err) {
                                                              cb(
                                                                err,
                                                                successLogs
                                                              );
                                                            } else {
                                                              let filenames =
                                                                data.split(
                                                                  "\n"
                                                                );
                                                              let executeCounter = 0;

                                                              setTimeout(() => {
                                                                filenames.forEach(
                                                                  (
                                                                    filename
                                                                  ) => {
                                                                    fs.unlink(
                                                                      `${folderPath}/${filename}`,
                                                                      (err) => {
                                                                        executeCounter++;
                                                                        if (
                                                                          err
                                                                        ) {
                                                                          failureLogs.push(
                                                                            err.message
                                                                          );
                                                                        } else {
                                                                          successLogs.push(
                                                                            `Deleted file ${filename}`
                                                                          );
                                                                        }
                                                                        if (
                                                                          executeCounter ===
                                                                          filenames.length
                                                                        ) {
                                                                          cb(
                                                                            failureLogs,
                                                                            successLogs
                                                                          );
                                                                        }
                                                                      }
                                                                    );
                                                                  }
                                                                );
                                                              }, 8000);
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
      }
    });
  }
};
